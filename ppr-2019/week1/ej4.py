n = int(input("Ingrese la cantidad de filas: "))
m = int(input("Ingrese la cantidad de columnas: "))
mat = [[None] * m for i in range(n)]
for i in range(n):
    cadena = str(input())
    if len(cadena) == m:
        for c in range(m):
            mat[i][c] = cadena[c]
    else:
        print("Usted tiene que ingresar", m, "caracteres")
for i in range(n):
    for j in range(m):
        cont = 0
        if mat[i][j] != "*":
            for l in range(i-1,i+2):
                for k in range(j-1,j+2):
                    if 0 <= l <= n-1 and 0 <= k <= m-1:
                        if mat[l][k] == "*":
                            cont += 1
            mat[i][j] = cont
for i in range(n):
    print(*mat[i])